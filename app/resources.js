export const Dimensions = {
    WIDTH: 1138,
    HEIGHT: 640
};

// ------------- DEFINING ASSETS --------------- //

export let ResourcesMap = new Map();

ResourcesMap.set('ship', {url: 'FlyingShip.png', h: 0, z: 37}); // z - to place it between clouds

ResourcesMap.set('background', [
    {url: 'cbbg_nightsky_bg.png', h: 0, z: 0},
    {url: 'cbbg_nightsky_bg_bottomglow.png', h: 0, z: 0},
  ]);

ResourcesMap.set('city', [
    {url: 'cbbg_nightsky_hc06.png', h: -10, o: 0, z: 32},
    {url: 'cbbg_nightsky_hc07.png', h: 0, o: 0, z: 30},
    {url: 'cbbg_nightsky_hc08.png', h: 20, o: 0, z: 25}, // not full size
    {url: 'cbbg_nightsky_hc09.png', h: 35, o: 0, z: 20}, // dust
    {url: 'cbbg_nightsky_hc10.png', h: 55, o: 0, z: 19}, // single
    {url: 'cbbg_nightsky_hc11.png', h: 55, o: Dimensions.WIDTH/2, z: 18}, // single
    {url: 'cbbg_nightsky_hc12.png', h: 25, o: 0, z: 18},
    {url: 'cbbg_nightsky_hc13.png', h: 45, o: 0, z: 15}, // not full size
    {url: 'cbbg_nightsky_hc14.png', h: 55, o: 0, z: 12}, // dust
    {url: 'cbbg_nightsky_hc15.png', h: 0, o: 0, z: 10},
  ]);

const delta = Dimensions.WIDTH/2;

ResourcesMap.set('foreground', [
    {url: 'cbbg_nightsky_hc01_blur.png', h: 0, o: 0, z: 60},
    {url: 'cbbg_nightsky_hc02_blur.png', h: 0, o: delta, z: 60},
    {url: 'cbbg_nightsky_hc03_blur.png', h: -20, o: 2*delta, z: 60},
    {url: 'cbbg_nightsky_hc04_blur.png', h: 0, o: 3*delta, z: 60},
    {url: 'cbbg_nightsky_hc05_blur.png', h: 0, o: 4*delta, z: 60},
  ]);
ResourcesMap.set('clouds', [
    {url: 'cbbg_nightsky_hc16.png', h: Dimensions.HEIGHT/2, o: 0, z: 40},
    {url: 'cbbg_nightsky_hc17.png', h: Dimensions.HEIGHT/4, o: Dimensions.WIDTH/3, z: 35},
    //{url: 'cbbg_nightsky_hc18.png', h: 0, o: 0, z: 0}, // TODO:
  ]);

// ----------- END OF DEFINIGN ASSETS ---------- //
