import 'pixi.js';

const SPEED_SCALE = 1/300;

export class ParallaxScene {
  constructor(renderer, scene, loader, width, height) {
    this.renderer = renderer;
    this.scene = scene;
    this.loader = loader;
    this.sprites = [];
    this.width = width;
    this.height = height;
  }

  addLayer(resources) {
    let createFunc = Array.isArray(resources) ? createLayerMany : createLayerOne;
    let sprite = createFunc.call(this, resources);
    this.sprites.push(sprite);
    this.scene.addChild(sprite);
  }

  addLayers(resources) {
    for(let res of resources) {
      let sprite = createLayerOne.call(this, res);
      this.sprites.push(sprite);
      this.scene.addChild(sprite);
    }
  }

  animate(deltaTime) {
    for(let sprite of this.sprites) {
      sprite.tilePosition.x -= sprite.zIndex * deltaTime*SPEED_SCALE;
    }
  }
}

function createLayerOne(res) {
    let sprite = null;
    let texture = this.loader.resources[res.url].texture;
    if(texture.width === this.width) {
      sprite = new PIXI.extras.TilingSprite(texture, this.width, texture.height);
      sprite.position.y = this.height- texture.height - res.h || 0;
    } else {
      let renderTexture = new PIXI.RenderTexture(this.renderer, this.width, texture.height);
      let childSprite = new PIXI.Sprite(texture);
      renderTexture.render(childSprite);
      sprite = new PIXI.extras.TilingSprite(renderTexture, this.width, texture.height);
      sprite.position.y = this.height - texture.height - res.h || 0;
      sprite.tilePosition.x = res.o || 0;
    }
    sprite.zIndex = res.z || 0;
    return sprite;
}

function createLayerMany(resourcesArray) {
  let renderTexture = new PIXI.RenderTexture(this.renderer, 2*this.width, this.height);
  let sprite = new PIXI.extras.TilingSprite(renderTexture, this.width, this.height);
  let spriteDoc = new PIXI.Container();

  for(let res of resourcesArray) {
    let texture = this.loader.resources[res.url].texture;
    let childSprite = new PIXI.Sprite(texture);
    childSprite.position.x = res.o || 0;
    childSprite.position.y = this.height - texture.height - res.h || 0;
    spriteDoc.addChild(childSprite);
    sprite.zIndex = res.z || 0;
  }
  renderTexture.render(spriteDoc);
  
  return sprite;
}
