import 'pixi.js';

export class ExhaustFilter extends PIXI.AbstractFilter {

  constructor() {
    let vertexShader = null;
    let fragmentShader = [
`
precision mediump float;
varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform float time;
uniform vec2 center;
uniform vec2 factor;
uniform vec4 color;

void main(void)
{
    vec4 pixel = texture2D(uSampler, vTextureCoord);
    vec2 coord = vTextureCoord.xy;

    //counting 1 / square of elipse dim

    float l_a2 = 1.0 / (center.x * center.x);
    float r_a2 = 1.0 / ((1.0-center.x) * (1.0-center.x));

    float dist2_l = l_a2 * (center.x-coord.x)*(center.x-coord.x) + 4.0 * (center.y-coord.y)*(center.y-coord.y);
    float dist2_r = r_a2 * (center.x-coord.x)*(center.x-coord.x) + 4.0 * (center.y-coord.y)*(center.y-coord.y);

    float dist2 = (coord.x < center.x) ? dist2_l : dist2_r;

    float color0 = 1.0 - 4.0 * abs((coord.x-center.x) * (coord.y-center.y));
    float color1 = 1.0 - dist2;
    float color2 = pow(color0, factor.x * (1.0+time)) * pow(color1, 1.0 + factor.y * (time));
    pixel = color * max(color2,0.0);
    gl_FragColor = pixel;
}
`
    ];
    let uniforms = {
      time: {
        type: '1f',
        value: 0
      },
      center: {
        type: 'v2',
        value: { x: 0.5, y: 0.5 }
      },
      factor: {
        type: 'v2',
        value: { x: 5.0, y: 1.0 }
      },
      color: {
        type: 'v4',
        value: { x: 1.0, y: 0.0, z: 1.0, w: 0.0 }
      }
    };
    super(
      vertexShader,
      fragmentShader,
      uniforms
    );
  }

  // this.uniforms is set by uniforms constructor
  get time() {
    return this.uniforms.time.value;
  }

  set time(value) {
    this.uniforms.time.value = value;
  }

  get center() {
    return this.uniforms.center.value;
  }

  set center(value) {
    this.uniforms.center.value = value;
  }

  get factor() {
    return this.uniforms.factor.value;
  }

  set factor(value) {
    this.uniforms.factor.value = value;
  }

  get color() {
    return this.uniforms.color.value;
  }

  set color(value) {
    this.uniforms.color.value = value;
  }
}
