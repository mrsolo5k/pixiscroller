import 'pixi.js';

import {Dimensions, ResourcesMap} from './resources';
import {ParallaxScene} from './parallaxScene';
import {Ship} from './ship';

window.onload = initialize;

function initialize() {

  const baseUrl = '../assets/';

  let lastTime = null;
  let isPaused = false;

  // ------------- SETTING UP RENDERER --------------- //

  let renderer = PIXI.autoDetectRenderer(Dimensions.WIDTH, Dimensions.HEIGHT);

  window.addEventListener("resize", resize);

  // Centering renderer
  renderer.view.style.position = 'absolute';
  renderer.view.style.left = '50%';
  renderer.view.style.top = '50%';
  renderer.view.style.transform = 'translate3d( -50%, -50%, 0 )';

  function resize() {
    let ratio = Math.min(window.innerWidth/Dimensions.WIDTH, window.innerHeight/Dimensions.HEIGHT);
    scene.scale.x = scene.scale.y = ratio;
    renderer.resize(Math.ceil(Dimensions.WIDTH * ratio), Math.ceil(Dimensions.HEIGHT * ratio));
  }

  // ------------- LOADING RESOURCES --------------- //

  let loader = PIXI.loader;
  let scene = new PIXI.Container();

  // require, load and create textures from images
  // using PIXI.loader to make sure images are loaded
  for (let [key, resGroup] of ResourcesMap) {
    resGroup = [].concat(resGroup);
    for(let res of resGroup) {
      let img = require('../assets/'+res.url);
      loader.add(res.url, img);
    }
  }

  loader.load(onLoaded);

  function onLoaded() {
    document.body.appendChild(renderer.view);
    resize();

    // Creating parallax scene
    let parallaxScene = new ParallaxScene(renderer, scene, loader, Dimensions.WIDTH, Dimensions.HEIGHT);
    parallaxScene.addLayer(ResourcesMap.get('background'));
    parallaxScene.addLayers(ResourcesMap.get('city'));
    parallaxScene.addLayers(ResourcesMap.get('clouds'));
    parallaxScene.addLayer(ResourcesMap.get('foreground'));

    // Adding ship to the scene
    let shipRes = ResourcesMap.get('ship');
    let ship = new Ship(loader.resources[shipRes.url].texture, scene);

    ship.zIndex = shipRes.z;
    ship.scale = { x: 0.67, y: 0.67 };

    scene.addChild(ship);

    // Adding Game Pause Text to the scene
    let pauseText = new PIXI.Text('GAME PAUSED',{font : '36px Arial', fill : 0xff00ff});
    pauseText.visible = false;
    pauseText.anchor = { x: 0.5, y: 0.5 };
    pauseText.position = { x: Dimensions.WIDTH/2, y: Dimensions.HEIGHT/2 };

    scene.addChild(pauseText);

    // Handling scene touch/click
    scene.addChild(createOnMouseClickLayer(()=> {
      isPaused = !isPaused;
      pauseText.visible = isPaused;
      ship.interactive = !isPaused;
    }));

    // Sorting each of created spites by z value
    scene.children.sort((a,b) => {
        a.zIndex = a.zIndex || 0;
        b.zIndex = b.zIndex || 0;
        return a.zIndex - b.zIndex;
    });

    animate();
    function animate() {
        requestAnimationFrame(animate);

        let now = Date.now();

        if(lastTime && !isPaused) {
          let deltaTime = now - lastTime;

          ship.animate(deltaTime);
          parallaxScene.animate(deltaTime);
        }

        renderer.render(scene);
        lastTime = now;
    }
  }
}

function createOnMouseClickLayer(onScreenClick) {
  let mouseLayer = new PIXI.Container();
  mouseLayer.interactive = true;
  mouseLayer.hitArea = new PIXI.Rectangle(0, 0, Dimensions.WIDTH, Dimensions.HEIGHT);
  mouseLayer.click = onScreenClick;
  return mouseLayer;
}
