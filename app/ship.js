import PIXI from 'pixi.js';
import {ExhaustFilter} from'./filters/exhaust';

const FALL_SPEED = -0.1;
const LIFT_SPEED = 1.0;
const DEVIATION_RATE = 1.4;
const DEVIATION_AMPL = 0.3;
const EXHAUST_RATE = 30;
const LIGHT_RATE = 1;

const SCALE = 1000;
const PHI = 0.5 * Math.PI;

const WIDTH = 1138;
const HEIGHT = 640;

export class Ship extends PIXI.Sprite {
  constructor(texture, scene, position) {
    super();

    this.boundsRect = new PIXI.Graphics();
    scene.addChild(this.boundsRect);

    this.filterFlare1 = new ExhaustFilter();
    this.filterFlare2 = new ExhaustFilter();
    this.filterExhaust = new ExhaustFilter();

    this.filterFlare1.factor = {x: 10.0, y: 100.0};
    this.filterFlare1.color = {x: 1.0, y: 0.0, z: 0.0, w: 0.0};
    this.filterFlare2.factor = {x: 10.0, y: 100.0};
    this.filterFlare2.color = {x: 0.0, y: 1.0, z: 0.0, w: 0.0};
    this.filterExhaust.center = {x: 0.8, y: 0.5};

    this.sprites = [];

    this.sprites.push(new PIXI.Sprite(texture));
    this.sprites.push(createFXSprite(texture, this.filterFlare1, { x: 60.0, y: 43.0 }, { x: 0.5, y: 0.5 }));
    this.sprites.push(createFXSprite(texture, this.filterFlare2, { x: 85.0, y: 230.0 }, { x: 0.5, y: 0.5 }));
    this.sprites.push(createFXSprite(texture, this.filterExhaust, { x: 80.0, y: 160.0 }, { x: 1.0, y: 0.25 }, { x: 0.8, y: 0.5 }));

    for(let elem of this.sprites) {
      this.addChild(elem);
    }

    this.interactive = true;
    this.mousedown = onClick.bind(this);

    this.speed = FALL_SPEED;
    this.position = position || { x: 200, y: 100 };
    this.localPostion = this.position;
    this.elapsedTime = 0;
  }

  animate(deltaTime) {

    this.elapsedTime += deltaTime;
    let flareBrightness = Math.abs(Math.sin(LIGHT_RATE*this.elapsedTime/SCALE));
    this.filterFlare1.time = Math.abs(flareBrightness);
    this.filterFlare2.time = Math.abs(flareBrightness);
    this.filterExhaust.time = Math.abs(Math.sin(EXHAUST_RATE*this.elapsedTime/SCALE) + PHI);

    let deviation = Math.sin((this.elapsedTime)*DEVIATION_RATE/SCALE) * DEVIATION_AMPL;

    this.position.y -= this.speed + deviation;

    if(this.speed > FALL_SPEED) {
      this.speed -= deltaTime/SCALE;
    } else {
      this.speed = FALL_SPEED;
    }
  }
}

function onClick() {
  this.speed = LIFT_SPEED;
}

function createFXSprite(texture, filter, pos, scale, anchor) {
  let sprite = new PIXI.Sprite(texture);
  sprite.shader = filter;

  sprite.position = pos || { x: 0.0, y: 0.0 };
  sprite.scale = scale || { x: 1.0, y: 1.0 };
  sprite.anchor = anchor || { x: 0.5, y: 0.5 };
  sprite.pivot = { x: 0.0, y: 0.0 };

  return sprite;
}
