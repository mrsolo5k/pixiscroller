var path = require('path')
var webpack = require('webpack')

// I'm running a very basic command to convert app.js => bundle.js. It looks like:
//
// webpack app.js bundle.js

module.exports = {
  entry: [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:8080',
    path.resolve(__dirname, 'app/app.js')
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  module: {
    preLoaders: [
      {
        test: /\.js?$/,
        loaders: ['jshint'],
        include: /app/,
      }
    ],
    loaders: [
      {
        test: /\.json$/,
        loader: 'json'
      },
      { 
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets:['es2015']
        }
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        loader: 'file'
      }
    ],
    postLoaders: [
      {
        include: path.resolve(__dirname, 'node_modules/pixi.js'),
        loader: 'transform?brfs'
      }
    ]
  }
}