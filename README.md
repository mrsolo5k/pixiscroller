#Introduction#

This is a stub of a parallax scrolling game. It is all written in JavaScript (ES6) with PIXI.js 2D rendering engine. Effects of blinking lights and engine exhaust are done with WebGL Shader which is placed in:

```
app/
  filters/
    exhaust.js
```

#Starting the App#


```
npm install
npm run build
npm run dev
```

Application will start by default on the port **8080**. To change it go to file **webpack.config.js**